package com.shiro.config.shiro;

import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.slf4j.Logger;
import org.apache.shiro.mgt.SecurityManager;
import org.slf4j.LoggerFactory;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.LinkedHashMap;
import java.util.Map;
import javax.servlet.Filter;

/**
 * Created by Administrator on 2018/9/1.
 */

@Configuration
public class ShiroConfig {

    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Bean
    public ShiroFilterFactoryBean shiroFilter(SecurityManager securityManager) {
        logger.info("===========================进入了shirFilter()");
        ShiroFilterFactoryBean shiroFilterFactoryBean = new ShiroFilterFactoryBean();

        // 必须设置 SecurityManager
        shiroFilterFactoryBean.setSecurityManager(securityManager);

        /** 看到这里我很生气，写这里之前我已经写好了一个权限控制的工具类，这家伙竟然和我的工具类长得很相似，很鸡肋 */
        // 拦截器.
        Map<String, String> filterMap = new LinkedHashMap<String, String>();
        //anon: 不需要授权也能访问的请求路径
        filterMap.put("/html/login", "anon");
        filterMap.put("/webPage/**", "anon");
        filterMap.put("/common/**", "anon");
        filterMap.put("/user/login", "anon");
        filterMap.put("/user/logout", "anon");

        //authc：需要授权才能访问的请求路径
        //filterMap.put("/user/**", "authc");//如果认证的拦截放在授权的前面，那么下面的授权将不会生效。
        //filterMap.put("/user/getUserNameList", "anon"); //注意顺序：一个请求路径能匹配多个时，细致的要放在前面。
        filterMap.put("/user/getUserList", "perms[获取用户信息]");
        //filterMap.put("/user/getUserList", "perms[获取用户信息2]");//相同的拦截配置后面的会覆盖掉前面的。
        filterMap.put("/user/addUser", "perms[添加用户]");
        filterMap.put("/**", "authc");

        //配置退出 过滤器,其中的具体的退出代码Shiro已经替我们实现了,所以连退出方法都不用写了
        filterMap.put("/user/logout", "logout");

        // 设置未认证时候跳转的登录页面，如果不设置默认会自动寻找Web工程根目录下的"/login.jsp"页面
        shiroFilterFactoryBean.setLoginUrl("/html/login");

        // 未授权界面;
        shiroFilterFactoryBean.setUnauthorizedUrl("/html/unpermission");

        // 登录成功后要跳转的链接
        // 查了许多相关资料，这个设置并不好使。
        //shiroFilterFactoryBean.setSuccessUrl("/html/index");
        LinkedHashMap<String, Filter> myMap = new LinkedHashMap<>();
        myMap.put("perms",myDefinedFilter());
        shiroFilterFactoryBean.setFilters(myMap);
        shiroFilterFactoryBean.setFilterChainDefinitionMap(filterMap);
        return shiroFilterFactoryBean;
    }


    @Bean
    public MyRealm myAuthRealm() {
        logger.info("============================进入了myShiroRealm()");
        MyRealm myAuthorizingRealm = new MyRealm();
        return myAuthorizingRealm;
    }


    @Bean
    public SecurityManager securityManager(){
        DefaultWebSecurityManager securityManager =  new DefaultWebSecurityManager();
        securityManager.setRealm(myAuthRealm());
        return securityManager;
    }

    @Bean
    public MyDefinedFilter myDefinedFilter(){
       return new MyDefinedFilter();
    }
}
