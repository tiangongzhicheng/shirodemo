package com.shiro.config.shiro;

import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.authz.AuthorizationFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

/**
 * Created by Administrator on 2018/9/7.
 */

@Configuration
public class MyDefinedFilter extends AuthorizationFilter {

    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue) throws Exception {

        logger.info("================进入了自定义过滤器");
        System.out.println("================进入了自定义过滤器");

        Subject subject = this.getSubject(request, response);
        String[] perms = ((String[])mappedValue);
        if(perms != null && perms.length > 0) {
            for (String perm : perms) {
                if(subject.isPermitted(perm)){
                    return  true;
                }
            }
        }else {
            //如果没有权限要求，放行
            return true;
        }

        return false;

    }
}
