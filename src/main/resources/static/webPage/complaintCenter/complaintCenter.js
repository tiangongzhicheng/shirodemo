/**
 * Created by wangyunzhe on 2018/1/25.
 */
var loaded = false;//防止执行重复
$(function(){
    if(!loaded){

        $('.btn_pos').on('tap',function(){
            window.location.href='../html/complaintCenterAuthentication';
        });

        getComplaintList();

        pushHistory();
        window.addEventListener("popstate", function(e) {
            window.location = '../html/debtManagementYes';  //根据自己的需求实现自己的功能
        }, false);
        function pushHistory() {
            var state = {
                title: "title",
                url: "#"
            };
            window.history.pushState(state, state.title, state.url);
        }

        $('#back').die().live('tap',function(){
            window.location = '../html/debtManagementYes';
        });
        loaded = true;
    }


});
/**
 * 获取投诉信息
 */
function getComplaintList(){
    $.ajax({
        url: '../complaint/getComplaintList',
        type:'post',
        data:{'debtorId':getCookie('debtorId')},
        dataType:'json',
        success:function (data) {
            if(data.success){
                var list = data.result.list;
                var html ='';
                if(list.length=='0'){
                    $('.complaint_no').removeClass('hide');
                    $('.complaint_wrapper').addClass('hide');
                }else{
                    $('.complaint_no').addClass('hide');
                    $('.complaint_wrapper').removeClass('hide');
                    for(var i=0;i<list.length;i++){
                        html += '<div class="item_box" id="'+list[i].id+'">'+
                            '<div class="avatar">'+getPic(list[i].picUrl)+'</div>'+
                            '<div class="avatar_text_box">'+
                            '<h5 class="black black_company">'+list[i].complaintTitle+'</h5>'+
                            '<h5 class="'+getColor(list[i].status)+'">'+list[i].status+'</h5>'+
                            '<h5 class="gray gray_date">'+list[i].createTime+'</h5>'+
                            '</div>'+
                            '</div>';
                    }
                    $('#complaint_wrapper').html(html);
                    jumpDetails();
                }

            }else{
                $.alert(data.message);
            }
        }
    });
}

function getColor(status){
    if(status=='处理中' || status=='待处理'){
        return 'red';
    }else if(status == '已解决' || status == ''){
        return 'blue';
    }else if(status == '已停止投诉'){
        return 'orange';
    }
}

function getPic(url) {
    if(url!=''){
        return '<img src="'+url+'" />';
    }else{
        return '<img src="/zichan360WeChatWeb/webPage/complaintCenter/avatar.png" />'
    }
}

function jumpDetails(){
    $('.item_box').on('tap',function(){
        addCookie('complainId',$(this).attr('id'),7);//把投诉id放到cookie中
        window.location.href = '../html/complaintDetails';
    });
}
