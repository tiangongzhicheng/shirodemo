/**
 * Created by wangyunzhe on 2018/1/26.
 */
var loaded = false;//防止执行重复
var isTrue = true;
var isTrueOk = true;
var myreg =/(^1[3|4|5|7|8][0-9]{9}$)/;//手机号码

$(function(){
    if(!loaded){
        getDebtorPhoneInWechat();
        /**
         * 点击获取验证码
         */
        $('#get_sms_code').die().live('tap',function () {
            getSmsCode('../sms/sendSmsCode');
        });

        /**
         * 点击确定
         */
        $('#check_sms_code').die().live('tap',function () {
            checkSmsCode('../sms/checkSms');
        });

        loaded = true;
    }

});
/**
 * 获取手机号
 */
function getDebtorPhoneInWechat(){
    $.ajax({
        url: '../debtor/getDebtorPhoneInWechat',
        type:'post',
        data:{'debtorId':getCookie('debtorId')},
        dataType:'json',
        success:function (data) {
           if(data.success){
               if(data.data!=null){
                   $('#phone').val(data.data);
                   $('#phone').attr('disabled',true);
               }else{
                   $('#phone').attr('disabled',false);
               }

           }

        }
    });


}
/**
 * 点击确定
 */
function checkSmsCode(url){
    if(isTrueOk){
        var phone = $('#phone').val();
        if(phone ==''){
            $.alert('请填写手机号');
            return false;
        }else if(!myreg.test(phone)){
            $.alert('请填写正确的手机号');
            return false;
        }

        var smsCode = $('#sms_code').val();
        if(smsCode ==''){
            $.alert('请填写短信验证码');
            return false;
        }else if(smsCode.length!=4 ){
            $.alert('验证码格式不正确');
            return false;
        }
        isTrueOk = false;
        $.ajax({
            url: url,
            type:'post',
            data: {phone:phone,smsCode:smsCode},
            dataType:'json',
            success:function (data) {
                isTrueOk = true;
                if(data.success){
                    $('.error-box').hide();
                    $.ajax({
                        url: '../debtor/addDebtorPhone',
                        type:'post',
                        data: {phone:phone,debtorId:getCookie('debtorId')},
                        dataType:'json',
                        success:function (data) {
                            if(data.success){
                                window.location.href='../html/fillInComplaintDetails';
                                return;
                            }else{
                                window.location.href='../html/error';
                            }
                        }
                    });
                }else{
                    //$.alert(data.message);
                    $('.error-box').show();
                    return;
                }
            }
        });
    }
}

/**
 * 点击获取验证码
 */

function getSmsCode(url){
    if(isTrue){
        var phone = $('#phone').val();
        if(phone ==''){
            $.alert('请填写手机号');
            return false;
        }else if(!myreg.test(phone)){
            $.alert('请填写正确的手机号');
            return false;
        }
        isTrue = false;
        var timeer;
        var time=60;
        $.ajax({
            url: url,
            type:'post',
            data: {phone:phone},
            dataType:'json',
            success:function (data) {
                if(data.success){
                    $('#get_sms_code').css({'background-color':'#a7a7a7'});
                    $('#get_sms_code').html('60s后重新获取');
                    timeer = setInterval(function(){
                        time--;
                        if(time==0){
                            $('#get_sms_code').css({'background-color': '#0e9fff'});
                            $('#get_sms_code').html('重新获取');
                            clearInterval(timeer);
                            isTrue = true;
                        }else{
                            $('#get_sms_code').css({'background-color':'#a7a7a7'});
                            $('#get_sms_code').html(time+'s后重新获取');
                        }
                    },1000);
                    return;
                }else{
                    isTrue = true;
                    $.alert(data.message);
                    return;
                }
            }
        });
    }
}