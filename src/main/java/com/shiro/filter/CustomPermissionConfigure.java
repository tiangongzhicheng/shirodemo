package com.shiro.filter;

import org.springframework.context.annotation.Configuration;

import java.util.*;

/**
 * Created by Administrator on 2018/8/30.
 */
@Configuration
public class CustomPermissionConfigure {

    public static List<String> unFilterUrlList = new ArrayList<>();
    static {
        //登录方法
        unFilterUrlList.add("/zichan360WeChatWeb/html/signIn");
        //页面
        //unFilterUrlList.add("/zichan360WeChatWeb/html");
        //静态资源
        unFilterUrlList.add("zichan360WeChatWeb/webPage/");
        unFilterUrlList.add("zichan360WeChatWeb/common/");
    }

    public static Map<String, String> map = new LinkedHashMap<>();
    static {
        map.put("/debtor/getDebtorInfoList","获取债务人列表");
        map.put("/debtor/addDebtorInfo","添加债务人");
        map.put("/debtor/deleteDebtorInfo","删除债务人");
        map.put("/debtor/updateDebtorInfo","修改债务人");
        map.put("/debtor/","债务人相关权限");

        map.put("/complaint/getComplaintList2","获取投诉列表");
        map.put("/complaint/addComplaint2","添加投诉");
        map.put("/complaint/deleteComplaint2","删除投诉");

        map.put("/html/department","查看部门,查看用户");
        map.put("/html/user","查看用户");
        map.put("/html/complaint","查看投诉");
    }

    public String getNeedPermiss(String url)  {
        Set<String> keys = map.keySet();
        for (String key : keys){
            if(url.startsWith(key)){
                return map.get(key);
            }
        }
        return null;
    }


    public static List<String> havePermiss = new ArrayList<>();
    static {
        havePermiss.add("添加用户");
        havePermiss.add("查看用户");

    }
}
