package com.shiro.pojo;

import com.alibaba.fastjson.JSON;

public class BaseResult<T> {

	private String message;
	private boolean success;
	private T result;


	public BaseResult() {
	}

	public BaseResult( String message, boolean success) {
		this.message = message;
		this.success = success;
	}

	public static BaseResult getSuccessRes() {
		return new BaseResult("成功",true);
	}

	public static BaseResult getFailRes() {
		return new BaseResult("失败",false);
	}
	public static BaseResult getFailRes(String message) {
		return new BaseResult(message,false);
	}


	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public boolean getSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public T getResult() {
		return result;
	}

	public void setResult(T result) {
		this.result = result;
	}

	public String toJsonString() {
		return JSON.toJSONString(this);
	}

}
