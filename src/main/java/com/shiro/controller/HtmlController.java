package com.shiro.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by Administrator on 2018/9/5.
 */
@Controller
@RequestMapping("/html")
public class HtmlController {

    @RequestMapping("/login")
    public String login(){
        return "login";
    }

    @RequestMapping("/index")
    public String index(){
        return "index";
    }

    @RequestMapping("/userModular")
    public String user(){
        return "user";
    }

    @RequestMapping("/department")
    public String department(){
        return "department";
    }

    @RequestMapping("/unpermission")
    public String unpermission(){
        return "unpermission";
    }
}
