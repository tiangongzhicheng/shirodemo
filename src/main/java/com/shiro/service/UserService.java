package com.shiro.service;

import com.shiro.mapper.UserMapper;
import com.shiro.pojo.BaseResult;
import com.shiro.pojo.User;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Administrator on 2018/9/1.
 */
@Service
public class UserService {

    @Autowired
    private UserMapper userMapper;

    public BaseResult login(User user) {
        // 1、获取Subject实例对象
        Subject subject = SecurityUtils.getSubject();

        if(!subject.isAuthenticated()){
            //.创建令牌 (用户名和密码的封装对象)
            UsernamePasswordToken token = new UsernamePasswordToken(user.getUserName(), user.getPassWord());

            try {
                // 传到MyRealm类中的方法进行认证
                //这句返回的是void，根据是否抛异常判断登录是否成功
                subject.login(token);
                Session session = subject.getSession();
                session.setAttribute("user",user);
            }catch (Exception  e){
                return BaseResult.getFailRes("用户名或者密码不正确");
            }
        }
        return BaseResult.getSuccessRes();
    }

    public User findByName(String username,String password) {
        User user = new User();
        user.setUserName(username);
        user.setPassWord(password);
        List<User> user1 = userMapper.getUser(user);
        if(user1 == null || user1.size()==0){
            return null;
        }
        return user1.get(0);
    }

    public BaseResult getUserList() {
        BaseResult successRes = BaseResult.getSuccessRes();
        successRes.setResult(userMapper.getUserList());
        return successRes;
    }

    public BaseResult addUser(User user) {
        if(StringUtils.isBlank(user.getUserName()) || StringUtils.isBlank(user.getPassWord())){
            return BaseResult.getFailRes("用户名/密码不能为空");
        }
        Integer integer = userMapper.addUser(user);
        if(integer > 0){
            return BaseResult.getSuccessRes();
        }
        return BaseResult.getFailRes();
    }

    public BaseResult getUserNameList() {
        BaseResult successRes = BaseResult.getSuccessRes();
        successRes.setResult(userMapper.getUserNameList());
        return successRes;
    }
}
