/**
 * Created by wangyunzhe on 2018/1/27.
 */
var loaded = false;//防止执行重复
$(function(){
    if(!loaded){
        getInstallmentGroupByMonth();

        loaded = true;
    }
});
/**
 * 查询该债务人的分期还款分布在哪几个月份
 */
function getInstallmentGroupByMonth(){
    $.ajax({
        url: '../debtor/getInstallmentGroupByMonth',
        type:'post',
        data:{'debtorId':getCookie('debtorId')},
        dataType:'json',
        success:function (data) {
            if(data.success){
                var list = data.data;
                var html ='';
                for(var i=0;i<list.length;i++){
                    html+='<div class="overView_box">'+
                        '<div class="title overView_title" tag="hide" id="overView_title'+i+'">'+
                        '<span class="black">'+list[i].monthNo+'</span>'+
                    '<i class="icon-drop_down_02 down" id="down'+i+'"></i>'+
                        '<i class="icon-stop_03 up hide" id="up'+i+'"></i>'+
                        '</div>'+
                        '<div class="content_box hide">'+
                        '<ul class="gray title_ul">'+
                        '<li>产品名称</li>'+
                        '<li>应还金额</li>'+
                       /* '<li>利息</li>'+
                        '<li>逾期费用</li>'+*/
                        '<li>最后还款日</li>'+
                        '</ul>'+
                        '<div class="value_box" id="value_box'+list[i].monthNo+'">'+
                    '</div>'+
                    '<div class="debt_all gray">'+
                        '<span>'+list[i].monthNo+'月应还：</span>'+
                    '<span><span class="red">￥</span><span class="red font18">'+list[i].repaySum+'</span></span>'+
                        '</div>'+
                        '</div>'+
                        '</div>';
                    getDebtInfoOverview(list[i].monthNo);
                }
                $('#overView_wrapper').html(html);
                getItemClick();
            }else{
                $.alert(data.message);
            }
        }
    });
}
/**
 * 获取债务总览
 */
function getDebtInfoOverview(mon){
    $.ajax({
        url: '../debtor/getDebtInfoOverview',
        type:'post',
        data:{'debtorId':getCookie('debtorId'),'mon':mon},
        dataType:'json',
        success:function (data) {
            if(data.success){
                var list = data.data;
                var ulHtml='';
                for(var i=0;i<list.length;i++){
                    ulHtml+='<ul class="black value_ul">'+
                        '<li><span class="mult-text">'+list[i].productName+'</span></li>'+
                        '<li class="red">'+list[i].repaymentMoney+'</li>'+
                        /*'<li>'+list[i].interest+'</li>'+
                        '<li>'+list[i].lateFee+'</li>'+*/
                        '<li>'+list[i].monthNo+'</li>'+
                        '</ul>';
                }
                $('#value_box'+mon).html(ulHtml);
            }else{
                $.alert(data.message);
            }
        }
    });
}
/**
 * 点击每个月份获取下拉
 */
function getItemClick(){
    for(var i = 0;i<$('.overView_box').length;i++){
        $('#overView_title'+i).die().live('tap',function(e){
            if($(this).attr('tag')=='hide'){
                $(this).siblings('.content_box').slideDown();
                $(this).attr('tag','show');
                $(this).find('.down').hide();
                $(this).find('.up').show();
            }else if($(this).attr('tag')=='show'){
                $(this).siblings('.content_box').slideUp();
                $(this).attr('tag','hide');
                $(this).find('.down').show();
                $(this).find('.up').hide();

            }
            e.stopPropagation();
        });

        $('#down'+i).die().live('tap',function(e){
            if($(this).parent().attr('tag')=='hide') {
                $(this).parent().siblings('.content_box').slideDown();
                $(this).parent().attr('tag', 'show');
                $(this).removeClass('icon-drop_down_02');
                $(this).addClass('icon-stop_03');
                //$(this).siblings('.up').show();
            }else if($(this).parent().attr('tag')=='show'){
                $(this).parent().siblings('.content_box').slideUp();
                $(this).parent().attr('tag', 'hide');
                $(this).addClass('icon-drop_down_02');
                $(this).removeClass('icon-stop_03');
            }
            e.stopPropagation();
        });

        $('#up'+i).die().live('tap',function(e){
            if($(this).parent().attr('tag')=='show') {
                $(this).parent().siblings('.content_box').slideUp();
                $(this).parent().attr('tag', 'hide');
                $(this).addClass('icon-drop_down_02');
                $(this).removeClass('icon-stop_03');
            }else if($(this).parent().attr('tag')=='hide'){
                $(this).parent().siblings('.content_box').slideDown();
                $(this).parent().attr('tag', 'show');
                $(this).removeClass('icon-drop_down_02');
                $(this).addClass('icon-stop_03');
            }
            e.stopPropagation();
        });

    }
}