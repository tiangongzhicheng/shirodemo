/**
 * Created by wangyunzhe on 2018/1/23.
 */
var loaded = false;//防止执行重复
$(function(){
    if(!loaded){
        getAuthenInfo('../debtor/getAuthenInfo');
        var isTrue = true;
        var myreg =/(^1[3|4|5|7|8][0-9]{9}$)/;//手机号码
        var mycall = /^((0\d{2,3}))(\d{7,8})?$/; //电话号码
        var idreg = /^(\d{15}$|^\d{18}$|^\d{17}(\d|X|x))$/; //身份证号码
        var china = /^[\u4e00-\u9fa5]+$/ ;//只允许是中文

        //点击确认
        $('#ok_btn').die().live('tap',function () {
            getConfirm('../debtor/authenDebtor');
        });

        //点击重新认证
        $('#re_ok_btn').die().live('tap',function () {
            getConfirm('../debtor/reAuthenDebtor');
        });


        pushHistory();
        window.addEventListener("popstate", function(e) {
            window.location = '../html/authen';  //根据自己的需求实现自己的功能
        }, false);
        function pushHistory() {
            var state = {
                title: "title",
                url: "#"
            };
            window.history.pushState(state, state.title, state.url);
        }


        function getConfirm(url){
            if(isTrue){
                if($('#idcardNo').val()==''){
                    reloadCaptcha($("#companyCaptcha"));
                    $.alert('请填写身份证号');
                    return false;
                } else
                if(isCardID($('#idcardNo').val()) !='true'){
                    reloadCaptcha($("#companyCaptcha"));
                    //$.alert('请填写合法的身份证号');
                    $.alert(isCardID($('#idcardNo').val()));
                    return false;
                }else
                if($('#name').val()==''){
                    reloadCaptcha($("#companyCaptcha"));
                    $.alert('请填写姓名');
                    return false;
                }else
                if($('#name').val()!='' && !china.test($('#name').val())){
                    reloadCaptcha($("#companyCaptcha"));
                    $.alert('请填写中文名字');
                    return false;
                }else
                if($('#code').val()==''){
                    reloadCaptcha($("#companyCaptcha"));
                    $.alert('请填写验证码');
                    return false;
                }else{
                    isTrue = false;
                    var obj = new Object();
                    obj.debtorName=$('#name').val();//债务人名字
                    obj.debtorIdcard=$('#idcardNo').val();//债务人身份证号
                    obj.captcha=$('#code').val();//验证码
                    $.ajax({
                        url: url,
                        type:'post',
                        data:obj,
                        dataType:'json',
                        success:function (data) {
                            isTrue = true;
                            //code 0 成功；-1为空;-2格式不正确;-3失败！;2为重复认证
                            reloadCaptcha($("#companyCaptcha"));
                            if(data.success){
                                if(data.code==0 || data.code==2){
                                    //$('.pop-div ,.pop_authentication_success').fadeIn();
                                    window.location.href='../html/debtManagementYes';
                                }
                                $('#name').val('');
                                $('#idcardNo').val('');
                                $('#code').val('');
                            }else{
                                if(data.code=='-1'){
                                    window.location.href='../html/debtManagementNo';
                                }else{
                                    //$('.pop-div ,.pop_authentication_fail').fadeIn();
                                    //$('#error').html(data.message);
                                    $.alert(data.message);
                                }
                            }
                        }
                    });
                }
            }
        }
        loaded = true;
    }

});
//身份号码的验证
var aCity={11:"北京",12:"天津",13:"河北",14:"山西",15:"内蒙古",21:"辽宁",22:"吉林",23:"黑龙江",31:"上海",32:"江苏",33:"浙江",34:"安徽",35:"福建",36:"江西",37:"山东",41:"河南",42:"湖北",43:"湖南",44:"广东",45:"广西",46:"海南",50:"重庆",51:"四川",52:"贵州",53:"云南",54:"西藏",61:"陕西",62:"甘肃",63:"青海",64:"宁夏",65:"新疆",71:"台湾",81:"香港",82:"澳门",91:"国外"}
function isCardID(sId){
    var iSum=0 ;
    var info="" ;
    if(!/^\d{17}(\d|x)$/i.test(sId)) return "你输入的身份证长度或格式错误";
    sId=sId.replace(/x$/i,"a");
    if(aCity[parseInt(sId.substr(0,2))]==null) return "你的身份证地区非法";
    sBirthday=sId.substr(6,4)+"-"+Number(sId.substr(10,2))+"-"+Number(sId.substr(12,2));
    var d=new Date(sBirthday.replace(/-/g,"/")) ;
    if(sBirthday!=(d.getFullYear()+"-"+ (d.getMonth()+1) + "-" + d.getDate()))return "身份证上的出生日期非法";
    for(var i = 17;i>=0;i --) iSum += (Math.pow(2,i) % 11) * parseInt(sId.charAt(17 - i),11) ;
    if(iSum%11!=1) return "你输入的身份证号非法";
    //aCity[parseInt(sId.substr(0,2))]+","+sBirthday+","+(sId.substr(16,1)%2?"男":"女");//此次还可以判断出输入的身份证号的人性别
    return 'true';
}
function getAuthenInfo(url){
    $.ajax({
        url: url,
        type:'post',
        data:'',
        dataType:'json',
        //contentType:'application/json',
        success:function (data) {
            isTrue = true;
            if(data.success){
                $('#name').val(data.data.debtorName);
                $('#idcardNo').val(data.data.debtorIdcard);
                //alert(data.data.debtorName);
            }


        }
    });
}


//图形验证码自动刷新
function reloadCaptcha(img) {
    var url;
    if (img.attr("src").indexOf("?") > 0) {
        url = img.attr("src").substring(0, img.attr("src").indexOf("?")) + '?' + Math.random();
    } else {
        url = img.attr("src") + '?' + Math.random();
    }
    img.attr("src", url);
}