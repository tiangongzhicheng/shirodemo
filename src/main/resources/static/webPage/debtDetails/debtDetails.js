/**
 * Created by wangyunzhe on 2018/1/25.
 */
var loaded = false;//防止执行重复
$(function(){
    if(!loaded){
        getDebtInfo();// 获取债务信息
        getDebtWithdraw();//获取债务列表
        loaded = true;
    }

});

/**
 * 获取债务信息
 */
function getDebtInfo(){
    $.ajax({
        url: '../debtor/getDebtInfo',
        type:'post',
        data:{'debtorId':getCookie('debtorId')},
        dataType:'json',
        success:function (data) {
            if(data.success){
                var result = data.data;
                var caseId = getCookie('caseId');
            for(var i=0;i<result.length;i++){
                if(caseId == result[i].caseId){
                    $('#productName').html(result[i].productName);//产品名称
                    $('#debtTotal').html(result[i].debtTotal);//当前逾期债务总额
                    $('#clientCompanyName').html(result[i].clientCompanyName);//借款公司
                }
            }
            }else{
                $.alert(data.message);
            }


        }
    });
}

/**
 * 获取分期列表
 */
function getDebtWithdraw(){
    $.ajax({
        url: '../debtor/getDebtWithdraw',
        type:'post',
        data:{'caseId':getCookie('caseId')},
        dataType:'json',
        success:function (data) {
            if(data.success){
                var result = data.data;
                var html = '';
                for(var i=0;i<result.length;i++){
                    html+='<div class="details_box" id="details_box'+i+'"><div class="submit_title black" withdrawId="'+result[i].withdrawId+'">提现'+(i+1)+'</div>'+
                        '<h5 class="numberPeriods_box">'+
                        '<span class="numberPeriods">共<span class="num">'+result[i].installmentCount+'</span>期</span>'+
                        '<span class="numberPeriods">已还<span class="num">'+result[i].outRepayCount+'</span>期</span>'+
                        '<span class="numberPeriods">未还<span class="num">'+result[i].inRepayCount+'</span>期</span>'+
                        '<span class="numberPeriods allDebt">债务总额<span class="num"><span class="font12">￥</span><span>'+result[i].debtTotal+'</span></span></span>'+
                        '</h5>'+
                        '<dl class="details_box_title details_box_title"><dt class="child">已还款账单<i class="active"></i></dt><dd class="child">未还款账单<i></i></dd></dl>'+
                        '<ul class="details_title_bar">'+
                        '<li>期数</li>'+
                        '<li>还款日</li>'+
                        '<li>金额</li>'+
                        '<li>状态</li>'+
                        '<li>查看详情</li>'+
                        '</ul>'+
                        '<div class="getDebtWithdraw_box">'+
                            '<div class="details_cont_box yes_order" style="display: block" id="withdraw_yes'+result[i].withdrawId+'"></div>'+
                            '<div class="details_cont_box no_order" id="withdraw_no'+result[i].withdrawId+'"></div>'+
                        '</div>'+
                        '<!--正常还款详情展示-->'+
                         '<div class="details_order_box normal" id="normal'+result[i].withdrawId+'"></div>'+
                        '<!--逾期已还详情展示-->'+
                        '<div class="details_order_box already" id="already'+result[i].withdrawId+'"></div>'+
                        '<!--逾期未还详情展示-->'+
                        '<div class="details_order_box no_have" id="no_have'+result[i].withdrawId+'"></div>'+
                        '<!--暂未到期详情展示-->'+
                        '<div class="details_order_box undue" id="undue'+result[i].withdrawId+'"></div>' +
                        '</div>';
                    getDebtInfoDetails(result[i].withdrawId);
                }
              $('#details_box').html(html);
                getPos();
            }else{
                $.alert(data.message);
            }


        }
    });
}
/**
 * 获取分期详情信息
 */
function getDebtInfoDetails(withdrawId){
    $.ajax({
        url: '../debtor/getDebtInfoDetails',
        type:'post',
        data:{'withdrawId':withdrawId},
        dataType:'json',
        success:function (data) {
            if(data.success){
                var list = data.data;
                var drawYesHtml='';
                var drawNoHtml='';
                var normalHtml='';
                var alreadyHtml='';
                var no_haveHtml='';
                var undueHtml='';
                for(var i=0;i<list.length;i++){
                    if(list[i].repaymentStatus=='正常还款' || list[i].repaymentStatus=='逾期还清'|| list[i].repaymentStatus=='延期还清'|| list[i].repaymentStatus=='提前还清'){
                        drawYesHtml += '<ul class="details_cont"><li>第'+list[i].termNo+'期</li><li>'+list[i].repaymentTime+'</li><li class="red">'+list[i].repaymentMoney+'</li><li>'+list[i].repaymentStatus+'</li><li><div class="btn btn_blue detail_btn" tag="'+list[i].repaymentStatus+'" id="detail_btn'+withdrawId+i+'">详情</div></li></ul>';
                    }else if(list[i].repaymentStatus=='暂未到期'|| list[i].repaymentStatus=='逾期未还'){
                        drawNoHtml += '<ul class="details_cont"><li>第'+list[i].termNo+'期</li><li>'+list[i].repaymentTime+'</li><li class="red">'+list[i].repaymentMoney+'</li><li>'+list[i].repaymentStatus+'</li><li><div class="btn btn_blue detail_btn" tag="'+list[i].repaymentStatus+'" id="detail_btn'+withdrawId+i+'">详情</div></li></ul>';
                    }

                }
                for(var i=0;i<list.length;i++){
                    if(list[i].repaymentStatus=='正常还款' || list[i].repaymentStatus=='提前还清'){
                        normalHtml +=
                            '<div id="child_nomal'+i+'" class="child_down"><h5><span>第<span class="red">'+list[i].termNo+'</span>期</span><i class="icon-stop_03" tag="'+list[i].repaymentStatus+'" id="icon-stop_03'+withdrawId+i+'"></i></h5>'+
                            '<h5><span>应还：<span class="red"><span class="yuan">￥</span><span>'+list[i].repaymentMoney+'</span></span></span><span class="real">实还：<span class="red2"><span class="yuan">￥</span><span>'+list[i].repaySum+'</span></span></span></h5>'+
                            '<h5><span>利息：<span class="red"><span class="yuan">￥</span><span>'+list[i].interest+'</span></span></span><span class="real">还款日期：<span class="black">'+list[i].repayTime+'</span></span></h5></div>';
                    }else if(list[i].repaymentStatus=='逾期还清' || list[i].repaymentStatus=='延期还清'){
                        alreadyHtml +=
                            '<div id="child_already'+i+'" class="child_down"><h5><span>第<span class="red">'+list[i].termNo+'</span>期</span><i class="icon-stop_03"  tag="'+list[i].repaymentStatus+'" id="icon-stop_03'+withdrawId+i+'"></i></h5>'+
                            '<h5><span>应还：<span class="red"><span class="yuan">￥</span><span>'+list[i].repaymentMoney+'</span></span></span><span class="real">实还：<span class="red2"><span class="yuan">￥</span><span>'+list[i].repaySum+'</span></span></span></h5>'+
                            '<h5><span>利息：<span class="red"><span class="yuan">￥</span><span>'+list[i].interest+'</span></span></span><span class="real">还款日期：<span class="black">'+list[i].repayTime+'</span></span></h5>'+
                            '<h5><span>逾期费：<span class="red"><span class="yuan">￥</span><span>'+list[i].lateFee+'</span></span></span></h5></div>';
                    }else if(list[i].repaymentStatus=='逾期未还'){
                        no_haveHtml +=
                            '<div id="child_no_have'+i+'" class="child_down"><h5><span>第<span class="red">'+list[i].termNo+'</span>期</span><i class="icon-stop_03" tag="'+list[i].repaymentStatus+'" id="icon-stop_03'+withdrawId+i+'"></i></h5>'+
                            '<h5><span>应还：<span class="red"><span class="yuan">￥</span><span>'+list[i].repaymentMoney+'</span></span></span><span class="real">实还：<span class="red2"><span class="yuan">￥</span><span>'+list[i].repaySum+'</span></span></span></h5>'+
                            '<h5><span>利息：<span class="red"><span class="yuan">￥</span><span>'+list[i].interest+'</span></span></span></h5>'+
                            '<h5><span>逾期费：<span class="red"><span class="yuan">￥</span><span>'+list[i].lateFee+'</span></span></span><span class="real">逾期天数：<span class="black">'+list[i].overDays+'</span></span></h5></div>';
                    }else if(list[i].repaymentStatus=='暂未到期'){
                        undueHtml +=
                            '<div id="child_undue'+i+'" class="child_down"><h5><span>第<span class="red">'+list[i].termNo+'</span>期</span><i class="icon-stop_03" tag="'+list[i].repaymentStatus+'" id="icon-stop_03'+withdrawId+i+'"></i></h5>'+
                            '<h5><span>应还：<span class="red"><span class="yuan">￥</span><span>'+list[i].repaymentMoney+'</span></span></span><span class="real">实还：<span class="red2"><span class="yuan">￥</span><span>'+list[i].repaySum+'</span></span></span></h5>'+
                            '<h5><span>利息：<span class="red"><span class="yuan">￥</span><span>'+list[i].interest+'</span></span></span><span class="real">还款日期：<span class="black">'+list[i].repayTime+'</span></span></h5></div>';
                    }

                }
                $('#withdraw_yes'+withdrawId).html(drawYesHtml);
                $('#withdraw_no'+withdrawId).html(drawNoHtml);
                $('#normal'+withdrawId).html(normalHtml);//正常还款或者提前还清
                $('#already'+withdrawId).html(alreadyHtml);//逾期还清或者延期还清
                $('#no_have'+withdrawId).html(no_haveHtml);//逾期未还
                $('#undue'+withdrawId).html(undueHtml);//暂未到期
                detailsClick(withdrawId,list.length);
            }else{
                $.alert(data.message);
            }


        }
    });
}

/**
 * 获取标题位置，tab切换
 */
function getPos(){
    var boxWidth = $('#details_box').width();
    var titleWidth = $('.submit_title').width();
    var left = (boxWidth-titleWidth)/2;
    $('.submit_title').css({'left':left});
    for(var i=0;i<$('#details_box .details_box').length;i++){
    // 已还款账单  未还款账单切换
        (function(num){
            $('#details_box'+num+' .details_box_title .child').die().on('tap',function(){
                $(this).find('i').addClass('active').parent().siblings('.child').find('i').removeClass('active');
                $('#details_box'+num+' .details_cont_box').eq($(this).index()).fadeIn().siblings('.details_cont_box').hide();
                $('#details_box'+num+' .child_down').hide();
            });
        })(i);
    }

}

/**
 * 点击详情
 */
function detailsClick(withdrawId,len){
    for(var i=0;i<len ;i++){
        (function(num){
            $('#detail_btn'+withdrawId+num).die().live('tap',function(){
                $(this).parents('.details_cont_box').hide();
                if($(this).attr('tag')=='正常还款'|| $(this).attr('tag')=='提前还清'){
                    $('#normal'+withdrawId).slideDown();
                    $('#normal'+withdrawId).find('#child_nomal'+num).show().siblings('.child_down').hide();
                    return false;
                }else if($(this).attr('tag')=='逾期还清' || $(this).attr('tag')=='延期还清'){
                    $('#already'+withdrawId).slideDown();
                    $('#already'+withdrawId).find('#child_already'+num).show().siblings('.child_down').hide();
                    return false;
                }else if($(this).attr('tag')=='逾期未还'){
                    $('#no_have'+withdrawId).slideDown();
                    $('#no_have'+withdrawId).find('#child_no_have'+num).show().siblings('.child_down').hide();
                    return false;
                }else if($(this).attr('tag')=='暂未到期'){
                    $('#undue'+withdrawId).slideDown();
                    $('#undue'+withdrawId).find('#child_undue'+num).show().siblings('.child_down').hide();
                    return false;
                }
            });
            $('#icon-stop_03'+withdrawId+num).die().live('tap',function(){
                if($(this).attr('tag')=='正常还款'|| $(this).attr('tag')=='提前还清'){
                    $('#normal'+withdrawId).hide();
                    $('#withdraw_yes'+withdrawId).slideDown();
                    return false;
                }else if($(this).attr('tag')=='逾期还清' || $(this).attr('tag')=='延期还清'){
                    $('#already'+withdrawId).hide();
                    $('#withdraw_yes'+withdrawId).slideDown();
                    return false;
                }else if($(this).attr('tag')=='逾期未还'){
                    $('#no_have'+withdrawId).hide();
                    $('#withdraw_no'+withdrawId).slideDown();
                    return false;
                }else if($(this).attr('tag')=='暂未到期'){
                    $('#undue'+withdrawId).hide();
                    $('#withdraw_no'+withdrawId).slideDown();
                    return false;
                }
            });
        })(i);
    }

}