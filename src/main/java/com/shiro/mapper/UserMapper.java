package com.shiro.mapper;

import com.shiro.pojo.User;
import org.apache.commons.lang.StringUtils;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.jdbc.SQL;

import java.util.List;

/**
 * Created by Administrator on 2018/9/1.
 */
@Mapper
public interface UserMapper {

    @SelectProvider(type=UserMapper.UserMapperProvider.class, method="getUser")
    List<User> getUser(User user);

    @Select("select * from shiro_user")
    List<User> getUserList();

    @Insert("INSERT INTO shiro_user (user_name,`password`) VALUES ( #{userName}, #{passWord}) ")
    @Options()
    Integer addUser(User user);

    @Select("select user_name from shiro_user")
    List<String> getUserNameList();


    class UserMapperProvider{
        public String getUser(User user){
            SQL sql = new SQL()
                    .SELECT("*")
                    .FROM("shiro_user");
            if(StringUtils.isNotBlank(user.getUserName())){
                sql.WHERE("user_name = #{userName}");
            }
            if(StringUtils.isNotBlank(user.getPassWord())){
                sql.WHERE("password = #{passWord}");
            }
            return sql.toString();
        }
    }
}
