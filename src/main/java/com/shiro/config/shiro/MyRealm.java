package com.shiro.config.shiro;

import com.shiro.config.Permission;
import com.shiro.pojo.User;
import com.shiro.service.UserService;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.ServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by Administrator on 2018/8/31.
 */
public class MyRealm extends AuthorizingRealm {

    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private UserService userService;

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        logger.info("执行了授权的方法");
        System.out.println("执行了授权的方法");
        /** 授权信息对象,类似于集合 */
        SimpleAuthorizationInfo authorizationInfo=new SimpleAuthorizationInfo();

        //获取主角对象
        User user = (User) principals.getPrimaryPrincipal();

        // 获取user对应的权限permission，将permission添加到authorizationInfo;

        /**这里本应该是从数据库中查询，方便演示，这里我从Map里面获取。*/
/*        Permission permissions = userService.getPermissionByUserId()
        for(Permission permission : permissions){
            authorizationInfo.addStringPermission(permission);//给当前用户赋予权限
        }*/
        /**由于每次访问受权限控制的路径都会执行该方法，频繁查询数据库并不好，在实际开发中可以将用户权限缓存到redis里面。
        要注意的是，每次变更用户权限后，要同时刷新redis里面的用户权限。*/
        List<String> userPermission = Permission.userPermission.get(user.getUserName());
        authorizationInfo.addStringPermissions(userPermission);
        return authorizationInfo;

    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
        logger.info("执行了身份验证方法");
        System.out.println("执行了身份验证方法");

        //转换为用户名密码令牌。
        UsernamePasswordToken userToken=(UsernamePasswordToken)token;
        //获得用户名
        String username = userToken.getUsername();
        //获得密码
        //char[] password = userToken.getPassword();
        String password = new String(userToken.getPassword());

        User user = userService.findByName(username,password);
        if(user == null){
            logger.info("用户名/密码错误");
            return null;
        }
        logger.info("认证成功");

        //参数1：principal 主角对象(身份对象)  会话管理器管理的对象
        //参数2：credentials 密码
        //参数3：realmName realm类的名称 固定写法
        return new SimpleAuthenticationInfo(user, password, getName());
    }
}
