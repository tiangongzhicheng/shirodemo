/**
 * Created by wangyz on 2018/1/24.
 */
var loaded = false;//防止执行重复
$(function() {
    if(!loaded){
        /**
         * 获取债务信息
         */
        getDebtInfo();


        /**
         * 点击投诉中心
         */
        $('#complaint_conter').die().live('tap',function () {
            window.location.href='../html/complaintCenter';
        });

        /**
         * 点击债务总览
         */
        $('#debt_overview').die().live('tap',function () {
            window.location.href='../html/debtOverView';
        });




        loaded = true;
    }

});

/**
 * 获取债务信息
 */
function getDebtInfo(){
    $.ajax({
        url: '../debtor/getDebtInfo',
        type:'post',
        data:{'debtorId':getCookie('debtorId')},
        dataType:'json',
        success:function (data) {
            if(data.success){
                var result = data.data;
                var html ='';
                if(result.length=='1'){
                    var k = 2;
                }else if(result.length=='2'){
                    var k = 2;
                }else{
                    var k = 1;
                }
                for(var i=0;i<result.length;i++){
                    html+='<li class="pic3" id="pic'+(i+k)+'">'+
                        '<h5 class="debt_title">'+result[i].productName+'</h5>'+
                        '<div class="circleProgress_wrapper">'+
                        '<div class="'+getStatus(result[i].caseStatus)+'">'+result[i].caseStatus+'</div>'+
                        '<div class="wrapper right">'+
                        '<div class="circleProgress '+getStatusCirle(result[i].caseStatus)+'"></div>'+
                        '</div>'+
                        '</div>'+
                        '<h5 class="account-title">账单金额</h5>'+
                        '<h4 class="account"><span>￥</span>'+result[i].debtTotal+'</h4>'+
                        '<h5 class="company-title">借款公司</h5>'+
                        '<h6 class="company">'+result[i].clientCompanyName+'</h6>'+
                        '<div class="btn btn_blue btn_detail" id="btn_detail'+(i+1)+'" caseId="'+result[i].caseId+'">查看详情</div>'+
                        '</li>';
                }
                $('#znsRotatePic').html(html);
                detailBtnClick();
                getSlider();
            }else{
                $.alert(data.message);
            }


        }
    });
}

/**
 * 债务区块儿左右滑动方法
 */
function getSlider(){
    //获取手机宽度  第二个滑块的位置
    var screenWidth = window.screen.width;
    var PicWidth =$('#pic2').width();
    var left = (screenWidth-PicWidth)/2;
    $('#pic2').css('left',left);

    var btnTop = $('#btn_detail2').offset().top;
    var btnLeft = $('#btn_detail2').offset().left;
    $('#hide_link').css({'top':btnTop,'left': btnLeft});
    var oUl = document.getElementById('znsRotatePic');
    var aLi = oUl.children;
    var oldMsg = [];
    for(var i=0;i<aLi.length;i++){
        oldMsg.push({left:aLi[i].offsetLeft,top:aLi[i].offsetTop,imgO:getStyle(aLi[i],'opacity'),aClick:aLi[i].onclick});
    }
    function goRight(){
        oldMsg.push(oldMsg.shift());
        changePos();
    }
    function goLeft(){
        oldMsg.unshift(oldMsg.pop());
        changePos();
    }
    function changePos(){
        for(var i=0;i<oldMsg.length;i++){
            move(aLi[i],{left:oldMsg[i].left,top:oldMsg[i].top});
            //move(aImg[i],{width:oldMsg[i].imgWidth,opacity:oldMsg[i].imgO,top:oldMsg[i].imgT});
            aLi[i].onclick = oldMsg[i].aClick;
        }
    }
    $('body').on('touchstart', '#znsRotatePic', function(e) {
        var touch = e.originalEvent;
        startX = e.originalEvent.changedTouches[0].pageX;
        startY = e.originalEvent.changedTouches[0].pageY;
        $("#znsRotatePic").on('touchmove', function(e) {
            //e.preventDefault();
            touch = e.originalEvent.touches[0] || e.originalEvent.changedTouches[0];
            if(touch.pageX - startX > 10) {
                goRight();
                //alert('右')
                $("#znsRotatePic").off('touchmove');

            } else if(touch.pageX - startX < -10) {
                goLeft();
                //alert('左')
                $("#znsRotatePic").off('touchmove');
            };
            if(touch.pageY - startY > 10) {
                //alert("下划");
            } else if(touch.pageY - startY < -10) {
                //alert("上划");
            };
        });
        return false;
    }).on('touchend', function() {
        $("#znsRotatePic").off('touchmove');
    });
}

/**
 * 区分已还清还是未还清 的样式显示方法
 */
function getStatus(caseStatus){
    if(caseStatus=='未还清'){
        return 'un_circle';
    }else{
        return 'already_circle';
    }
}
function getStatusCirle(caseStatus){
    if(caseStatus=='未还清'){
        return 'rightcircle';
    }else{
        return 'leftcircle';
    }
}

/**
 * 点击查看详情
 */
function detailBtnClick(){
    $('.btn_detail').on('touchstart ',function () {
        window.location.href='../html/debtDetails';
        addCookie('caseId',$(this).attr('caseId'),7);
    });
}

