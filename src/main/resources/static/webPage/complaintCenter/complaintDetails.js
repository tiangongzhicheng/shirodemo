/**
 * Created by Administrator on 2018/1/27.
 */
var loaded = false;//防止执行重复
$(function(){
    if(!loaded){
        var boxWidth = $('#submit_box').width();
        var titleWidth = $('#submit_title').width();
        var left = (boxWidth-titleWidth)/2
        $('.submit_title').css({'left':left});

        getComplaintList();//获取投诉信息
        getReplyInfo();//刷新回复消息
        setInterval(function(){
            getReplyInfo();//定时刷新回复消息
        },60000);
        //发送消息
        $('#send_out').on('tap',function () {
            addReplyInfo();
        });
        //点击停止投诉
        $('#btn_stop').on('tap',function () {
            $('.pop-div,.stop_pop').fadeIn();
        });
        //点击已解决
        $('#btn_solve').on('tap',function () {
            $('.pop-div,.solve_pop').fadeIn();
        });
        //是否停止投诉-->是
        $('#yes_stop_btn').on('tap',function () {
            $('.pop-div,.stop_pop').fadeOut();
            updateComplaintStatus($(this).attr('tag'));
        });
        //是否停止投诉-->否
        $('#no_stop_btn').on('tap',function () {
            $('.pop-div,.stop_pop').fadeOut();
        });
        //是否已解决-->是
        $('#btn_solve_yes').on('tap',function () {
            $('.pop-div,.solve_pop').fadeOut();
            updateComplaintStatus($(this).attr('tag'));
        });
        //是否停止投诉-->否
        $('#btn_solve_no').on('tap',function () {
            $('.pop-div,.solve_pop').fadeOut();
        });

        $('#back').die().live('tap',function(){
            window.location = '../html/complaintCenter';
        });
        loaded = true;
    }

});

/**
 * 获取投诉信息
 */
function getComplaintList(){
    $.ajax({
        url: '../complaint/getComplaintList',
        type:'post',
        data:{'debtorId':getCookie('debtorId'),'id':getCookie('complainId')},
        dataType:'json',
        success:function (data) {
            if(data.success){
                var result = data.result.list;
                for(var i=0;i<result.length;i++){
                    if(result[i].status=='待处理'){
                        $('#progress1').addClass('active');
                        $('#progress2').addClass('active');
                        $('#progress3').removeClass('active');
                        $('#progress4').removeClass('active');
                        $('#progressTest1').addClass('active');
                        $('#progressTest2').addClass('active');
                        $('#progressTest3').removeClass('active');
                        $('#progressTest4').removeClass('active');

                        $('#submit_box').show();//已提交
                        $('#follow_box').show();//已跟进
                        $('#reply_box').hide();//已回复
                        $('#solve_box').hide();//已解决
                        $('#btn_box').show();//按钮（停止投诉  已解决）
                        $('#name_input_box').show();//按钮 回复消息

                    }else if(result[i].status=='处理中'){
                        $('#progress1').addClass('active');
                        $('#progress2').addClass('active');
                        $('#progress3').addClass('active');
                        $('#progress4').removeClass('active');
                        $('#progressTest1').addClass('active');
                        $('#progressTest2').addClass('active');
                        $('#progressTest3').addClass('active');
                        $('#progressTest4').removeClass('active');

                        $('#submit_box').show();//已提交
                        $('#follow_box').show();//已跟进
                        $('#reply_box').show();//已回复
                        $('#solve_box').hide();//已解决
                        $('#btn_box').show();//按钮（停止投诉  已解决）
                        $('#name_input_box').show();//按钮 回复消息
                    }else{
                        $('#progress1').addClass('active');
                        $('#progress2').addClass('active');
                        $('#progress3').addClass('active');
                        $('#progress4').addClass('active');
                        $('#progressTest1').addClass('active');
                        $('#progressTest2').addClass('active');
                        $('#progressTest3').addClass('active');
                        $('#progressTest4').addClass('active');
                        $('#submit_box').show();//已提交
                        $('#follow_box').show();//已跟进
                        if($('#response-box').html()==''){
                            $('#reply_box').hide();//已回复
                        }else{
                            $('#reply_box').show();//已回复
                        }

                        $('#solve_box').show();//已解决
                        $('#btn_box').hide();//按钮（停止投诉  已解决）
                        $('#name_input_box').hide();//按钮 回复消息
                        if(result[i].status=='已解决'){
                            $('#already_p').html('投诉问题已经解决。');
                        }else if(result[i].status=='已停止投诉'){
                            $('#already_p').html('投诉问题已停止投诉。');
                        }else if(result[i].status=='无效投诉'){
                            $('#already_p').html('投诉问题是无效投诉。');
                        }
                    }
                    //已提交
                    $('#complain_title').html(result[i].complaintTitle);//投诉标题
                    $('#complain_obj').html(result[i].complaintPlatform);//投诉对象
                    $('#complain_money').html(result[i].relateMoney);//涉及金额
                    $('#complain_details').html(result[i].content);//事件详情
                    $('#data_sub').html(result[i].createTime);//投诉时间
                    //已跟进
                    $('#platform').html(result[i].complaintPlatform);//投诉对象
                    $('#follow_time').html(result[i].createTime);//已跟进
                    $('#solve_time').html(result[i].updateTime);//已解决

                    var imgHtml ='';
                    if(result[i].picUrl!=''){
                        var picUrlArr = [];
                        picUrlArr = result[i].picUrl.split(",");
                        for(var j=0;j<picUrlArr.length;j++){
                            imgHtml += '<img src="'+picUrlArr[j]+'" id="img_view'+j+'"/>';
                        }
                        $('#img_box').html(imgHtml);
                        getView(0);
                        getView(1);
                        getView(2);
                    }else{
                        $('#img_box').html('');
                    }

                }

            }else{
                $.alert(data.message);
            }
        }
    });
}
/**
 * 图片预览
 */
function getView(num) {
    $('#img_view'+num).on('tap',function () {
        $('.pop-div,.img_pop').fadeIn();
        $('#img_big').html('<img class="img_big" src="'+$(this).attr('src')+'" />');
    });
}

/**
 * 更改投诉状态
 */
function updateComplaintStatus(status){
    var obj = new Object();
    obj.id= getCookie('complainId');//投诉消息id
    obj.status = status;//要修改的状态：1-待处理；2-处理中；3-已经决；4-已停止投诉；5-无效投诉',
    obj.remark = '';//备注信息（可填可不填）
    $.ajax({
        type:"post",
        url:"../complaint/updateComplaintStatus",
        data: obj,
        dataType:'json',
        success:function (data) {
            if(data.success){
                getComplaintList();//获取投诉信息
            }else{
                alert(data.message);
            }

        }
    });
}

/**
 * 刷新回复消息
 */
function getReplyInfo(){
    var obj = new Object();
    obj.complaintId = getCookie('complainId');//投诉消息id
    obj.status = '0';//是否要将消息状态变为已读；0-不改变，1-改变为已读
    $.ajax({
        type: "post",
        url: "../complaint/getReplyInfo",
        data: obj,
        dataType: "json",
        success: function (data) {
            if(data.success){
                //source:消息来自哪一方；0-用户方；1-平台方
                var result = data.data;
                var html ='';
                for(var i=result.length-1;i>=0;i--){
                    if(result[i].source=='0'){
                        html+='<div class="customer_say"><span class="gray title">回复平台：</span><p class="black content">'+result[i].content+'</p></div>';
                    }else{
                        html+='<div class="company_say"><span class="gray title">平台回复：</span><p class="black content">'+result[i].content+'</p></div>';
                    }
                    $('#replay_time').html(result[0].createTime);
                }
                $('#response-box').html(html);

            }else{
                alert(data.message);
            }
        }
    });
}

/**
 * 添加交谈信息
 */
function addReplyInfo(){
    if($('#inp_re').val()==''){
        $.alert('请填写消息内容！');
        return false;
    }
    var obj = new Object();
    obj.complaintId = getCookie('complainId');//投诉消息id
    obj.debtorId = getCookie('debtorId');//债务人id
    obj.content = $('#inp_re').val();//消息内容
    obj.source = 0;//消息来自哪一方；0-用户方；1-平台方
    $.ajax({
        type: "post",
        url: "../complaint/addReplyInfo",
        data: obj,
        dataType: "json",
        success: function (data) {
            if(data.success){
                getReplyInfo();
                $('#inp_re').val('');
            }
        }
    });
}