package com.shiro.config;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2018/9/1.
 */
public class Permission {


    public static List<String> adminPermission = new ArrayList<>();
    static {
        adminPermission.add("获取用户信息");
        adminPermission.add("添加用户");
    }

    public static List<String> rootPermission = new ArrayList<>();
    static {
        rootPermission.add("获取用户信息");
    }

    public static Map<String,List> userPermission = new HashMap<>();
    static {
        userPermission.put("admin",adminPermission);
        userPermission.put("root",rootPermission);
    }
}
