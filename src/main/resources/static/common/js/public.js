/**
 * Created by wyz on 2018/1/24.
 */
$(function(){
    $('.close').on('tap',function () {
       $('.pop-div,.pop').fadeOut();
    });

   /* $('#back').on('tap',function () {
        window.location.href='../html/debtManagementYes';
    });*/
    //债务id  947945
});
//数据验证
$('input[type=text]').live('focusout',function (event) {  // 所有input  type=text 不能输入空格
    this.value = this.value.replace(/\s/g, '');
});
$('textarea').live('focusout',function (event) {  // 所有input  type=text 不能输入空格
    this.value = this.value.replace(/\s/g, '');
});
$('.isNanFloat').live('keyup', function () {  //判断输入不是数字的 并且准许输入 小数点
    this.value = this.value.replace(/[^\-?\d.]/g, '');
});

function addCookie(name,value,iDay){
    //判断生存周期
    if(iDay){
        var oDate = new Date();
        oDate.setDate(oDate.getDate()+iDay);
        oDate.setHours(0,0,0,0);
        document.cookie = name+'='+value+'; PATH=/; EXPIRES='+oDate.toGMTString();
    }else{
        document.cookie = name+'='+value+'; PATH=/';
    }
}

function getCookie(sName){
    var arr = document.cookie.split('; ');
    for(var i=0;i<arr.length;i++){
        var arr2= arr[i].split('=');
        if(arr2[0]==sName){
            return arr2[1];
        }
    }
}

function removeCookie(sName){
    addCookie(sName,1,-1);
}
//addCookie('debtorId',254874,7);