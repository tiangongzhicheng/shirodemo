/**
 * Created by wangyunzhe on 2018/1/26.
 */
var loaded = false;//防止执行重复
var meryNum = /^\d+(?:\.\d{1,2})?$/; //两位小数
var isTrueF = true;
$(function(){
    if(!loaded){
        $('.hidden_upload').parent().addClass('file-box');


        getColpainObj();//获取投诉对象


        /**
         * 点击图片上的叉号
         */
        $('#upload .close').die().live('tap',function(e){
            e.stopPropagation();
            var _this =  $(this);
            $(this).parents('.img_box').hide();
            $(this).parents('.img_box').siblings('.eg_box').show();
            setTimeout(function () {
                _this.parents('.img_box').siblings('.eg_box').find('input').show();
            },100);
        });

        /**
         * 点击确定按钮
         */
        $('#add_btn').on('tap',function () {
            addComplaint();
        });

        /**
         * 点击确定按钮
         */
       /* $('#re_btn_ok').on('tap',function () {
            $('.pop-div  , .pop_authentication_success').fadeOut();
            // 跳转投诉闲情页面complaintDetails.html
            window.location.href = '../html/complaintDetails';
        });*/

        upPhoto(1);//第一个图片上传
        upPhoto(2);//第二个图片上传
        upPhoto(3);//第三个图片上传

        pushHistory();
        window.addEventListener("popstate", function(e) {
            window.location = '../html/complaintCenter';  //根据自己的需求实现自己的功能
        }, false);
        function pushHistory() {
            var state = {
                title: "title",
                url: "#"
            };
            window.history.pushState(state, state.title, state.url);
        }



        loaded = true;
    }

});

/**
 * 获取投诉对象
 */
function getColpainObj(){
    $.ajax({
        url: '../complaint/getClientCompanyList',
        type:'post',
        data:{'debtorId':getCookie('debtorId')},
        dataType:'json',
        success:function (data) {
            var list = data.data;
           var html ='';
           for(var i=0;i<list.length;i++){
               html+='<option  value ="'+list[i].name+'">'+list[i].name+'</option>';
           }
           $('#drpPublisher2').html(html);

        }
    });


}
/**
 * 图片上传
 */
function upPhoto(num){
    $('#hidden_upload'+num).live("change",function(){
        if (checkImgType(this)) {
            // 提交表单
            $('#upload'+num).ajaxSubmit({
                dataType: 'json',
                success: function (data) {
                    if (data.success) {
                        $('#imgBox'+num+'_inner').html('<img src="' + data.data + '" id="img'+num+'">');
                        $('#imgBox'+num).css({'display':'inline-block'});
                        $('#eg_box'+num).hide();
                        $('#eg_box'+num+' input').hide();
                        getView(num);
                    } else {
                        alert('上传失败!');
                    }
                },
                complete : function(XMLHttpRequest,status){ //请求完成后最终执行参数
                    if(status=='error'){//超时,status还有success,error等值的情况
                        //ajaxTimeoutTest.abort();
                        $.alert("由于网络慢，上传超时，请重新上传！");
                    }
                }
            });
        }
        // 为了防止普通浏览器进行表单提交和产生页面导航（防止页面刷新？）返回false
        return false;

    });
}

/**
 * 添加投诉信息
 */
function addComplaint() {
    if(isTrueF){

    if($('#fillIn_title').val()==''){//投诉标题
        $('#fillIn_title_error span').html('请填写投诉标题');
        $('#fillIn_title_error').show();
        return false;
    }else{
        $('#fillIn_title_error').hide();
    }

    if($('#drpPublisher2').val()==''){//投诉对象
        $('#fillIn_obj_error span').html('请选择投诉对象');
        $('#fillIn_obj_error').show();
    }else{
        $('#fillIn_obj_error').hide();
    }

   if($('#fillIn_money').val()==''){//涉及金额
        $('#fillIn_money_error span').html('请填写涉及金额');
        $('#fillIn_money_error').show();
        return false;
    }else if($('#fillIn_money').val()>100000){//涉及金额
        $('#fillIn_money_error span').html('涉及金额最多不能超过100000元');
        $('#fillIn_money_error').show();
       return false;
    }else if(!meryNum.test($('#fillIn_money').val())){//涉及金额
        $('#fillIn_money_error span').html('涉及金额最多只能保留两位小数');
        $('#fillIn_money_error').show();
       return false;
    }else{
       $('#fillIn_money_error').hide();
   }

   if($('#fillIn_cont').val()=='' || $('#fillIn_cont').val().length<20){//事件详情及要求
       $('#fillIn_cont_error span').html('字数不得少于20');
        $('#fillIn_cont_error').show();
        return false
    }else{
       $('#fillIn_cont_error').hide();
   }
   var urlStr = '';
   if($('#img1').attr('src')=='' && $('#img2').attr('src')==''  && $('#img3').attr('src')=='' ){
       urlStr ='';
   }else if($('#img1').attr('src')!='' && $('#img2').attr('src')==''  && $('#img3').attr('src')=='' ){
       urlStr =$('#img1').attr('src');//1
   }else if($('#img1').attr('src')!='' && $('#img2').attr('src')!=''  && $('#img3').attr('src')==''){
       urlStr =$('#img1').attr('src')+','+$('#img2').attr('src');//1,2
   }else if($('#img1').attr('src')!='' && $('#img2').attr('src')!=''  && $('#img3').attr('src')!=''){
       urlStr =$('#img1').attr('src')+','+$('#img2').attr('src')+','+$('#img3').attr('src');//1,2,3
   }else if($('#img1').attr('src')!='' && $('#img2').attr('src')==''  && $('#img3').attr('src')!=''){
       urlStr =$('#img1').attr('src')+','+$('#img3').attr('src');//1,3
   }else if($('#img1').attr('src')=='' && $('#img2').attr('src')!=''  && $('#img3').attr('src')!=''){
       urlStr =$('#img2').attr('src')+','+$('#img3').attr('src');//2,3
   }else if($('#img1').attr('src')=='' && $('#img2').attr('src')!=''  && $('#img3').attr('src')==''){
       urlStr =$('#img2').attr('src');//2
   }else if($('#img1').attr('src')=='' && $('#img2').attr('src')==''  && $('#img3').attr('src')!=''){
       urlStr =$('#img3').attr('src');//3
   }
   var obj = new Object();
   obj.debtorId = getCookie('debtorId');//债务人id
   obj.complaintTitle = $('#fillIn_title').val();//投诉标题
   obj.complaintPlatform = $('#drpPublisher2').val();//投诉对象
   obj.relateMoney = $('#fillIn_money').val();//涉及金额
   obj.content = $('#fillIn_cont').val();//内容
   obj.picUrl = urlStr;//图片路径
        isTrueF= false;
   $.ajax({
       url: '../complaint/addComplaint',
       type:'post',
       data:obj,
       dataType:'json',
       success:function (data) {
            if(data.success){
                //$('.pop-div , .pop_authentication_success').fadeIn();
                addCookie('complainId',data.id,7);//把投诉id放到cookie中
                window.location.href = '../html/complaintDetails';
                isTrueF= true;
            }else{
                $.alert(data.message);
                isTrueF= true;
            }
       }
   });
    }
}
/**
 * 图片预览
 */
function getView(num) {
    $('#img'+num).on('tap',function () {
        $('.pop-div,.img_pop').fadeIn();
        $('#img_big').html('<img class="img_big" src="'+$(this).attr('src')+'" />');
    });
}
/**
 * 检验上传图片的格式
 */
var isIE = /msie/i.test(navigator.userAgent) && !window.opera;
function checkImgType(target) {
    var fileSize = 0;
    if (target.value == "") {
        alert("请上传图片");
        return false;
    } else if (!/\.(jpg|png|JPG|PNG|bmp|jpeg|JPEG)$/.test(target.value)) {
        alert("图片类型必须是jpg,png,bmp,jpeg中的一种");
        target.value = "";
        return false;
    } else if (isIE && !target.files) {
        var filePath = target.value;
        var fileSystem = new ActiveXObject("Scripting.FileSystemObject");
        var file = fileSystem.GetFile(filePath);
        fileSize = file.Size;
    } else {
        fileSize = target.files[0].size;
    }
    var size = fileSize / 1024;
    if (size > 5 * 1024) {
        $.alert("不能大于5M");
        return false;
    }
    return true;
}

