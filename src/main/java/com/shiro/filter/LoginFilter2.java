package com.shiro.filter;

import com.alibaba.druid.util.StringUtils;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import javax.servlet.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;


//@WebFilter(filterName = "loginFilter", urlPatterns = "/*")
public class LoginFilter2 implements Filter {
    private Logger logger = LoggerFactory.getLogger(getClass());

    private static final String USERID_COOKIE_NAME = "userId";

    private static final String CONTEXT_PATH  = "/shiro";

    @Resource
    CustomPermissionConfigure customPermissionConfigure;


    /**
     * 是否需要拦截
     * @param url
     * @return
     */
    private boolean isFilter(String url){
        for(String s: CustomPermissionConfigure.unFilterUrlList) {
            if(url.indexOf(s) > -1) {
                return false;
            }
        }
        return true;
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        try {
            HttpServletRequest request = (HttpServletRequest) servletRequest;
            logger.error("requestURL======================================" + request.getRequestURL().toString());
            if(isFilter(request.getRequestURL().toString())) {
                String userId = getUserIdFromCookie(request.getCookies(),USERID_COOKIE_NAME);
                if (userId == null) {
                    JSONObject result = new JSONObject();
                    result.put("success", false);
                    result.put("message", "Please log in first");
                    servletResponse.getWriter().write(result.toJSONString());
                    throw new Exception("请先登录");
                }
            }
            if(checkPermiss(request.getRequestURL().toString())){
                filterChain.doFilter(servletRequest, servletResponse);
            }else {
                servletResponse.getWriter().write("not have permission ");
                throw new Exception("没有权限");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {

        }
    }

    @Override
    public void destroy() {


    }

    public Boolean checkPermiss(String url)  {
        //获取需要的权限
        int start = url.indexOf(CONTEXT_PATH)+CONTEXT_PATH.length();
        String reqUrl = url.substring(start, url.length());
        System.out.println(reqUrl);
        String s = customPermissionConfigure.getNeedPermiss(reqUrl);
        System.out.println("需要的权限：=="+s);

        if(s == null){
            return true;
        }
        //将需要的权限装到集合里面
        List<String> needPerms = Arrays.asList(s.split(","));

        /** 从数据查询具备的权限getPermissByUserId */
        List<String> havePerms = CustomPermissionConfigure.havePermiss;

        if(url.endsWith(".html")){
            //如果是.html页面则需要具备所有需要的权限才可访问
            for (String needPerm : needPerms) {
                if(!havePerms.contains(needPerm)){
                    return false;
                }
            }
        }else {
            //如果访问的是方法，则只要具备所需权限的一种就能访问
            for (String needPerm : needPerms) {
                if(havePerms.contains(needPerm)){
                    return true;
                }
            }
            return false;
        }
        return true;
    }

    private String getUserIdFromCookie(Cookie[] cookies, String key){
        if(cookies == null){
            return null;
        }
        for(Cookie cookie:cookies){
            if(StringUtils.equalsIgnoreCase(cookie.getName(), key)){
                try {
                    return cookie.getValue();
                } catch (NumberFormatException e) {
                    return null;
                }
            }
        }
        return null;
    }


}
