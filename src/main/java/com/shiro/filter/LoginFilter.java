package com.shiro.filter;

import com.alibaba.fastjson.JSONObject;
import com.shiro.pojo.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;


//@WebFilter(filterName = "loginFilter", urlPatterns = "/*")
public class LoginFilter implements Filter {
    private Logger logger = LoggerFactory.getLogger(getClass());


    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        try {
            HttpServletRequest request = (HttpServletRequest) servletRequest;
            logger.error("requestURL======================================" + request.getRequestURL().toString());
            User user = (User)((HttpServletRequest) servletRequest).getSession().getAttribute("user");

            if (user == null) {
                JSONObject result = new JSONObject();
                result.put("success", false);
                result.put("message", "Please log in first");
                servletResponse.getWriter().write(result.toJSONString());
                throw new Exception("请先登录");
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {

        }
    }

    @Override
    public void destroy() {


    }



}
