package com.shiro.controller;

import com.shiro.pojo.BaseResult;
import com.shiro.pojo.User;
import com.shiro.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Administrator on 2018/9/1.
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping("/login")
    public BaseResult login(User user){
        return userService.login(user);
    }

    @RequestMapping("/logout")
    public void logout(){
    }

    @RequestMapping("/getUserList")
    public BaseResult getUserList(){
        return userService.getUserList();
    }
    @RequestMapping("/getUserNameList")
    public BaseResult getUserNameList(){
        return userService.getUserNameList();
    }
    @RequestMapping("/addUser")
    public BaseResult addUser(User user){
        return userService.addUser(user);
    }
}
